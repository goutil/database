/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package postgres

const SchemaQuery = `
select m.tabname,
       json_object_agg(
               m.colname,
               json_build_object(
                       'descr', m.descr, 'type_name', m.type_name, 'type_descr', m.type_descr,
                       'maxlen', m.maxlen, 'defval', m.defval, 'ord', m.ord, 'nullable', m.nullable,
                       'is_pk', m.is_pk, 'is_fk', m.is_fk, 'fk_table', m.fk_table, 'fk_column', m.fk_column
                   )
           ) as "fields"
from (select ist.table_name                                                                  as tabname,
             isc.column_name                                                                 as colname,
             pg_catalog.col_description(isc.table_name::regclass::oid, isc.ordinal_position) as descr,
             isc.data_type                                                                   as type_name,
             isc.udt_name                                                                    as type_descr,
             isc.character_maximum_length                                                    as maxlen,
             isc.column_default                                                              as defval,
             isc.ordinal_position                                                            as ord,
             case when isc.is_nullable = 'YES' then TRUE else FALSE end                      as nullable,
             coalesce(keys.pk, false)                                                        as is_pk,
             coalesce(keys.fk, false)                                                        as is_fk,
             keys.fk_table,
             keys.fk_column
      from information_schema.columns isc
               join information_schema.tables ist
                    on ist.table_name = isc.table_name
                        and ist.table_type = 'BASE TABLE'
                        and ist.table_schema = 'public'
               left join (
          select tabname,
                 colname,
                 max(fk_table)  as fk_table,
                 max(fk_column) as fk_column,
                 bool_or(pk)    as pk,
                 bool_or(fk)    as fk
          from (select ccu.table_name     tabname,
                       ccu.column_name    colname,
                       ccu.table_name  as fk_table,
                       ccu.column_name as fk_column,
                       true            as pk,
                       false           as fk
                from information_schema.constraint_column_usage ccu
                         join information_schema.table_constraints tc
                              on ccu.constraint_name = tc.constraint_name
                                  and tc.table_name = ccu.table_name
                                  and ccu.table_schema = 'public'
                                  and tc.constraint_type = 'PRIMARY KEY'

                union

                select kcu.table_name,
                       kcu.column_name,
                       ccu.table_name,
                       ccu.column_name,
                       false,
                       true
                from information_schema.key_column_usage kcu
                         join information_schema.table_constraints tc
                              on kcu.constraint_name = tc.constraint_name
                                  and tc.table_name = kcu.table_name
                                  and kcu.table_schema = 'public'
                                  and tc.constraint_type = 'FOREIGN KEY'
                         join information_schema.constraint_column_usage ccu
                              on ccu.constraint_name = tc.constraint_name
               ) as keys

          group by tabname, colname) as keys
                         on keys.tabname = isc.table_name
                             and keys.colname = isc.column_name
      order by tabname, ord) m
group by tabname
`

const ModelsTeplate  = `
{{ with $g := . -}}
package {{ $g.PkgName }}

{{ if len $g.Imports -}}
import (
	{{ range $ii, $str := $g.Imports }}"{{$str}}"{{- end }}
)
{{ end }}

{{ range $tn, $t := $g.Schema.Tables }}
type {{ Normalize $tn true }} struct {
	tableName struct{} {{ $t.Tag }}
	{{ range $fn, $f := $t.Fields -}}
	{{ Normalize $fn false }} {{ $f.Type }} {{ $t.Tag $fn }}
	{{end}}
}
{{ end -}}

{{ end }}
`
