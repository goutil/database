/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package postgres

import (
	"strings"

	"gitlab.com/c2n2/goutil/logger"
)

func goType(t string) string {
	switch t {
	case "smallint":
		return "int8"
	case "integer":
		return "int32"
	case "bigint":
		return "uint64"
	case "real":
		return "float32"
	case "double precision":
		return "float64"
	case "boolean":
		return "bool"
	case "char", "character varying", "varchar", "text", "uuid":
		return "string"
	case "bytea":
		return "[]byte"
	case "jsonb":
		return "struct{}"
	case "date", "time", "timestamp", "timestamptz":
		return "time.Time"
	case "interval":
		return "time.Duration"
	case "inet":
		return "net.IP"
	case "cidr":
		return "net.IPNet"
	default:
		logger.Fatalf("Unsupported postgres type: %s", t)
		return ""
	}
}

func typeFromDescr(descr string) (typ string, imp string) {
	if strings.Contains(descr, "@golang:") {
		src := strings.Split(descr, ":")[1]
		t, s := strings.Split(src, "/"), strings.Split(src, ".")
		// type
		typ = t[len(t)-1]
		// import
		if len(s) > 1 {
			imp = s[0]
		}
	}
	return
}