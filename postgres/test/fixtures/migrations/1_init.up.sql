create schema public;

comment on schema public is 'standard public schema';

alter schema public owner to tester;

create table if not exists roles
(
    uid uuid not null
        constraint roles_pk
            primary key,
    name varchar(150) not null,
    is_super boolean
);

alter table roles owner to tester;

create table if not exists users
(
    uid uuid not null
        constraint users_pk
            primary key,
    email varchar(250) not null,
    password varchar(60) not null,
    reg_date timestamp with time zone default now() not null,
    nickname varchar(100),
    primary_role uuid
        constraint users_roles_uid_fk
            references roles
            on update set null on delete set null
);

comment on column users.reg_date is '@golang:time.Time';

alter table users owner to tester;

create unique index if not exists users_email_uindex
    on users (email);

create unique index if not exists users_uid_uindex
    on users (uid);

create unique index if not exists roles_name_uindex
    on roles (name);

create unique index if not exists roles_uid_uindex
    on roles (uid);
