/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package postgres

import (
	"bytes"
	"go/format"
	"os"
	"path"
	"strings"
	"text/template"

	"gitlab.com/c2n2/goutil/logger"
	"gitlab.com/c2n2/goutil/stuff"
)

const DefaultPackageName = "main"

type generator struct {
	PkgName string
	Schema  *Schema
}

func (g *generator) Imports() (imp []string) {
	imp = make([]string, 0)
	//
	imports := map[string]string{}
	for _, t := range g.Schema.Tables {
		for _, f := range t.Fields {
			if i := f.Import(); i != "" {
				if _, ok := imports[i]; !ok {
					imp = append(imp, i)
				} else {
					imports[i] = ""
				}
			}
		}
	}
	//
	return
}

/*
GenerateModels. Generates models from current database state
Args: 0 - package fname
*/
func GenerateModels(fpath string, args ...interface{}) {
	//
	var (
		buff bytes.Buffer
	)
	//
	schema, err := getSchema()
	if err != nil {
		logger.Fatalf("Failed to get database schema: %v", err)
	}

	g := &generator{
		PkgName: stuff.AgrOrDefault(args, 0, DefaultPackageName).String(),
		Schema:  schema,
	}

	tpl, err := template.New("model-generator").Funcs(template.FuncMap{
		"Normalize": NormalName,
	}).Parse(ModelsTeplate)
	if err != nil {
		logger.Fatalf("Filed to parse models template: %v", err)
	}

	// fill buffer
	if err := tpl.Execute(&buff, g); err != nil {
		logger.Fatalf("Filed to generate models: %v", err)
	}

	// formating buf
	file_cont, err := format.Source(buff.Bytes())
	if err != nil {
		logger.Fatalf("Failed to format models.go: %v", err)
	}

	mfile, err := os.Create(path.Join(fpath, "models.go"))
	if err != nil {
		logger.Fatalf("Filed to create models file: %v", err)
	}
	defer mfile.Close()

	if _, err := mfile.Write(file_cont); err != nil {
		logger.Fatalf("Failed to save models file: %v", err)
	}
}

/// Stuff

func NormalName(src string, rmsuffix bool) (res string) {
	res = src
	// remove suffix
	if rmsuffix {
		if strings.HasSuffix(res, "s") {
			res = res[:len(src)-1]
		}
	}
	// capitalize and join
	words := strings.Split(res, "_")
	for i, w := range words {
		words[i] = strings.ToUpper(string(w[0])) + w[1:]
	}
	res = strings.Join(words, "")
	return
}
