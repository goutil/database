/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package postgres

import (
	"fmt"
	"strings"

	"gitlab.com/c2n2/goutil/stuff"
)

/// Database schema

type Schema struct {
	Tables map[string]*SchemaTable `json:"tables"`
}

func getSchema() (*Schema, error) {
	// create empty schema
	schema := &Schema{Tables: make(map[string]*SchemaTable)}
	//
	rows := []*SchemaTable{}
	res, err := Query(&rows, SchemaQuery)
	if err != nil {
		return nil, err
	}
	// fill schema
	if res.RowsAffected() > 0 {
		for _, row := range rows {
			schema.Tables[row.Name] = row
		}
	}
	//
	return schema, nil
}

/// Table schema

type SchemaTable struct {
	Name   string                  `json:"tabname" sql:"tabname"`
	Fields map[string]*SchemaField `json:"fields" sql:"fields"`
}

func (t *SchemaTable) Tag(fields ...string) string {
	if len(fields) == 0 {
		return "`" + fmt.Sprintf(`sql:"%s"`, t.Name) + "`"
	}
	//
	if field, ok := t.Fields[fields[0]]; ok {
		name := fields[0]
		ext := stuff.NewFieldExtension().
			AddTag("json", name).
			AddTag("sql", name)
		// primary key
		if field.IsPK {
			ext.AddTag("sql", "pk")
		}
		// type
		typ := field.TypeDescr
		if field.Maxlen != nil {
			typ += fmt.Sprintf("(%d)", *field.Maxlen)
		}
		ext.AddTag("sql", fmt.Sprintf("type:%s", typ))
		// default value
		if field.Defval != "" {
			ext.AddTag("sql", fmt.Sprintf("default:%s", field.Defval))
		}
		// nullable
		if !field.Nullable {
			ext.AddTag("sql", "notnull")
		}
		return ext.String()
	}
	return ""
}

/// Field schama

type SchemaField struct {
	Descr     string `json:"descr"`
	TypeName  string `json:"type_name"`
	TypeDescr string `json:"type_descr"`
	Maxlen    *int   `json:"maxlen"`
	Defval    string `json:"defval"`
	Ord       int    `json:"ord"`
	Nullable  bool   `json:"nullable"`
	IsPK      bool   `json:"is_pk"`
	IsFK      bool   `json:"is_fk"`
	FkTable   string `json:"fk_table"`
	FkColumn  string `json:"fk_column"`
}

func (f *SchemaField) Type() string {
	var t string
	if f.Nullable {
		t = "*"
	}
	if typ, _ := typeFromDescr(f.Descr); typ != "" {
		t += typ
	} else {
		t += goType(f.TypeName)
	}
	return t
}

func (f *SchemaField) Import() string {
	//
	if _, imp := typeFromDescr(f.Descr); imp != "" {
		return imp
	}
	//
	if t := goType(f.TypeName); strings.Contains(t, ".") {
		return strings.Split(t, ".")[0]
	}
	//
	return ""
}